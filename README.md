José
=========
Welcome to José! José is a multi-function Discord bot made with Python and discord.py.

Requirements
==========
- (Preferrably) A Linux system
- Docker
- Docker Compose

Installation
============
You can just copy and paste this probably:
```bash
git clone https://gitlab.com/luna/jose.git
cd jose

cp example_config.py joseconfig.py

# fill in stuff from the example config file
# as you wish, etc
nano joseconfig.py

# docker should set everything up.
sudo docker-compose up
```

Help
=====

[Bot OAuth URL](https://discordapp.com/oauth2/authorize?permissions=379968&scope=bot&client_id=202586824013643777)

[Discord server](https://discord.gg/5ASwg4C)
