import collections
import asyncio
import logging
import re

import asyncpg
import discord

from discord.ext import commands
from discord.raw_models import RawReactionActionEvent

from .common import Cog

log = logging.getLogger(__name__)

# muh regex
IMAGE_REGEX = re.compile(r'(https?:\/\/.*\.(?:png|jpeg|jpg|gif|webp))',
                         re.M | re.I)
ID_REGEX = re.compile(r'\d+', re.M | re.I)
EMOJI_REGEX = re.compile(r'<a?:\w+:(\d+)>', re.I)

DEFAULT_STAR_EMOJI = '\N{WHITE MEDIUM STAR}'


class StarColor:
    """:releaseColoure:"""
    BLUE = 0x5dadec
    BRONZE = 0xc67931
    SILVER = 0xC0C0C0
    GOLD = 0xD4AF37
    RED = 0xff0000
    WHITE = 0xffffff


class StarAddError(Exception):
    """Error when adding a star to a message"""
    pass


class StarRemoveError(Exception):
    """Error when removing a star"""
    pass


class StarError(Exception):
    """General error"""
    pass


def get_humans(message) -> int:
    """Get all humans in a guild."""
    humans = sum(1 for m in message.guild.members if not m.bot)

    # Since selfstarring isn't allowed,
    # we need to remove 1 from the total amount.
    humans -= 1

    return max(1, humans)


def make_color(stars: int, message) -> int:
    """Generate a color for a embed depending on its star ratio."""
    color = 0x0
    star_ratio = stars / get_humans(message)

    if star_ratio >= 0:
        color = StarColor.BLUE
    if star_ratio >= 0.1:
        color = StarColor.BRONZE
    if star_ratio >= 0.2:
        color = StarColor.SILVER
    if star_ratio >= 0.4:
        color = StarColor.GOLD
    if star_ratio >= 0.8:
        color = StarColor.RED
    if star_ratio >= 1:
        color = StarColor.WHITE

    return color


def get_emoji(stars, message) -> str:
    """Generate a jose star emoji depending on its star ratio."""
    emoji = ''
    star_ratio = stars / get_humans(message)

    if star_ratio >= 0:
        emoji = '<:josestar1:353997747772456980>'
    if star_ratio >= 0.1:
        emoji = '<:josestar2:353997748216922112>'
    if star_ratio >= 0.2:
        emoji = '<:josestar3:353997748288225290>'
    if star_ratio >= 0.4:
        emoji = '<:josestar4:353997749341126657>'
    if star_ratio >= 0.8:
        emoji = '<:josestar5:353997749949300736>'
    if star_ratio >= 1:
        emoji = '<:josestar6:353997749630402561>'

    return emoji


def make_star_embed(stars: int, message):
    """Create the starboard embed."""
    star_emoji = get_emoji(stars, message)
    embed_color = make_color(stars, message)

    title = (f'{stars} {star_emoji} '
             f'{message.channel.mention}, ID: {message.id}')

    content = message.content
    em = discord.Embed(description=content, colour=embed_color)
    em.timestamp = message.created_at

    au = message.author
    avatar = au.avatar_url or au.default_avatar_url
    em.set_author(name=au.display_name, icon_url=avatar)

    # check for image urls
    search_res = IMAGE_REGEX.search(content)
    if search_res:
        em.set_image(url=search_res.group(0))

    attch = message.attachments
    if attch:
        attch_url = attch[0].url
        if attch_url.lower().endswith((
                'png',
                'jpeg',
                'jpg',
                'gif',
        )):
            em.set_image(url=attch_url)
        else:
            attachments = '\n'.join([
                f'[Attachment | {attch_s.filename}]({attch_s.url})'
                for attch_s in attch
            ])
            em.description += f'\n{attachments}'

    em.add_field(name='Jump URL', value=message.jump_url)
    return title, em


def check_nsfw(guild, config, message):
    """Check NSFW rules on channels and the current starboard."""
    starboard = guild.get_channel(config['starboard_id'])
    if starboard is None:
        raise StarError('No starboard found')

    nsfw_starboard = starboard.is_nsfw()
    nsfw_message = message.channel.is_nsfw()
    if nsfw_starboard:
        return

    if nsfw_message:
        raise StarError('NSFW message in SFW starboard')


class Starboard(Cog, requires=['config']):
    """Starboard.

    lol starboard u kno the good shit
    """

    def __init__(self, bot):
        super().__init__(bot)
        self.bot.simple_exc.extend([StarError, StarAddError, StarRemoveError])

        # prevent race conditions on all starboard operations
        self._locks = collections.defaultdict(asyncio.Lock)

        # janitor
        #: the janitor semaphore keeps things up and running
        #  by only allowing 1 janitor task each time.
        #  a janitor task cleans stuff out of mongo
        self.janitor_semaphore = asyncio.Semaphore(1)

    async def _del_star(self, message_id: int, guild_id: int):
        await self.pool.execute("""
        DELETE FROM starboard
        WHERE message_id = $1
          AND guild_id = $2
        """, message_id, guild_id)

    async def get_starconfig(self, guild_id: int) -> dict:
        """Get a starboard configuration object for a guild.

        If the guild is blocked, deletes the starboard configuration.
        """

        if await self.bot.is_blocked_guild(guild_id):
            guild = self.bot.get_guild(guild_id)

            res = await self.pool.execute("""
            DELETE FROM starconfig
            WHERE guild_id = $1
            """, guild.id)

            log.info(f'Deleted {res!r} g:`{guild.name} {guild.id}`'
                     'from blocking')
            return

        row = await self.pool.fetchrow("""
        SELECT guild_id, starboard_id, star_emoji, star_threshold, enabled
        FROM starboard_config
        WHERE guild_id = $1
        """, guild_id)

        try:
            drow = dict(row)
            if not drow['enabled']:
                return None

            return drow
        except TypeError:
            return None

    async def _get_starconfig(self, guild_id: int) -> dict:
        """Same as :meth:`Starboard.get_starconfig` but raises `StarError` when
        no configuration is found.
        """
        cfg = await self.get_starconfig(guild_id)
        if not cfg:
            raise StarError('No starboard configuration was '
                            'found for this guild')

        return cfg

    async def get_star(self, guild_id: int, message_id: int) -> dict:
        """Get a star object from a guild+message ID pair."""
        row = await self.pool.fetchrow("""
        SELECT message_id, channel_id, guild_id, author_id, star_message_id
        FROM starboard
        WHERE guild_id = $1 AND message_id = $2
        """, guild_id, message_id)

        return dict(row) if row is not None else None

    async def starrer_count(self, message_id: int) -> int:
        count = await self.pool.fetchval("""
        SELECT COUNT(*)
        FROM starboard_starrers
        WHERE message_id = $1
        """, message_id)

        return count or 0

    async def get_starrers(self, message_id: int) -> list:
        starrers = await self.pool.fetch("""
        SELECT starrer_id
        FROM starboard_starrers
        WHERE message_id = $1
        """, message_id)

        return [r['starrer_id'] for r in starrers]

    async def star_exist(self, message_id: int, guild_id: int):
        chan_id = await self.pool.fetchval("""
        SELECT channel_id
        FROM starboard
        WHERE message_id = $1 AND guild_id = $2
        """, message_id, guild_id)
        return chan_id is not None

    async def raw_add_star(self, config: dict, message: discord.Message,
                           author_id: int):
        """Add a star to a message."""
        guild_id = config['guild_id']
        guild = message.guild

        check_nsfw(guild, config, message)

        exist = await self.star_exist(message.id, guild_id)

        if not exist:
            await self.pool.execute(
                """
                INSERT INTO starboard
                    (message_id, channel_id, guild_id, author_id)
                VALUES ($1, $2, $3, $4)
                """,
                message.id, message.channel.id, message.guild.id,
                message.author.id
            )

        try:
            await self.pool.execute("""
            INSERT INTO starboard_starrers (message_id, starrer_id)
            VALUES ($1, $2)
            """, message.id, author_id)
        except asyncpg.UniqueViolationError:
            raise StarAddError('Already starred')

    async def raw_remove_star(self, config: dict, message: discord.Message,
                              author_id: int):
        """Remove a star from someone, updates the star object
        in the starboard collection.
        """
        guild_id = config['guild_id']
        star = await self.get_star(guild_id, message.id)
        if star is None:
            raise StarRemoveError('No message starred to be unstarred')

        res = await self.pool.execute("""
        DELETE FROM starboard_starrers
        WHERE message_id = $1 AND starrer_id = $2
        """, message.id, author_id)

        if res == 'DELETE 0':
            raise StarRemoveError("Author didn't star the message.")

    async def raw_remove_all(self, config: dict,
                             message: discord.Message):
        """Remove all starrers from a message."""
        guild_id = config['guild_id']

        star = await self.get_star(guild_id, message.id)
        if star is None:
            raise StarError('Star object not found to be reset')

        await self.pool.execute("""
        DELETE FROM starboard_starrers
        WHERE message_id = $1
        """, message.id)

    async def debug_log(self, msg: str, message: discord.Message):
        """Send a debug log call with the star as a context."""
        channel_id = message.channel.id
        guild_id = message.guild.id
        chan = self.bot.get_channel(channel_id)
        guild = self.bot.get_guild(guild_id)

        stars = await self.starrer_count(message.id)

        log.debug(f'{msg}\n'
                  f'{stars} starrers right now\n'
                  f'message {message}\n'
                  f'channel "{chan}" {channel_id}\n'
                  f'guild "{guild}" {guild_id}')

    async def delete_starobj(self, message_id: int, guild_id: int, msg=None):
        """Delete a star object from the starboard collection.
        Removes the message from starboard if provided.
        """
        if msg:
            await msg.delete()

        await self._del_star(message_id, guild_id)

    async def _star_embed(self, message: discord.Message) -> tuple:
        stars = await self.starrer_count(message.id)
        title, embed = make_star_embed(stars, message)
        return title, embed

    async def starboard_send(self, starboard: discord.TextChannel,
                             message: discord.Message) -> discord.Message:
        """Sends a message to the starboard."""
        title, embed = await self._star_embed(message)
        return await starboard.send(title, embed=embed)

    async def _try_starmsg(self, starboard: discord.TextChannel,
                           star_message_id: int):
        if star_message_id is None:
            return

        try:
            return await starboard.get_message(star_message_id)
        except (KeyError, discord.errors.NotFound):
            return

    async def update_star(self, config: dict, message: discord.Message,
                          delete_mode=False):
        """Update a star.

        Posts it to the starboard, edits if a message already exists.

        Parameters
        ----------
        config: dict
            Starboard configuration for the guild.
        delete: bool, optional
            If this should delete the star.
        msg: discord.Message
            A message object reffering to the star.

        Raises
        ------
        StarError
            For any error that happened while updating that star.
        """

        guild_id = config['guild_id']
        guild = self.bot.get_guild(guild_id)
        if not guild:
            raise StarError('No guild found with the starboard configuration')

        starboard = guild.get_channel(config['starboard_id'])
        if not starboard:
            await self.delete_starconfig(config)
            raise StarError('No starboard channel found')

        star_message_id = await self.pool.fetchval("""
        SELECT star_message_id
        FROM starboard
        WHERE message_id = $1
        """, message.id)

        star_message = await self._try_starmsg(starboard, star_message_id)
        starcount = await self.starrer_count(message.id)

        threshold = config.get('star_threshold', 1)
        below_threshold = starcount < threshold
        above_threshold = starcount >= threshold

        if delete_mode:
            await self.delete_starobj(message.id,
                                      guild_id, msg=star_message)
            return

        if below_threshold and star_message is not None:
            await star_message.delete()

        # do update/send here
        # if it isnt above threshold, we shouldn't do anything
        if not above_threshold:
            return

        await self.debug_log('boop update', message)

        if star_message is None:
            star_message = await self.starboard_send(starboard, message)

            await self.pool.execute("""
            UPDATE starboard
            SET star_message_id = $1
            WHERE message_id = $2
            """, star_message.id, message.id)
        else:
            title, embed = await self._star_embed(message)
            await star_message.edit(content=title, embed=embed)

    async def add_star(self,
                       message: discord.Message,
                       author_id: int,
                       config: dict = None):
        """Add a star to a message.

        Parameters
        ----------
        message: `discord.Message`
            Message to be starred.
        author_id: int
            Author ID of the star.

        Raises
        ------
        StarAddError
            If any kind of error happened while adding the star.
        """
        lock = self._locks[message.guild.id]
        await lock

        try:
            if not config:
                config = await self._get_starconfig(message.guild.id)

            await self.check_allow(config, message.channel.id)

            if hasattr(author_id, 'id'):
                author_id = author_id.id

            if author_id == message.author.id:
                raise StarAddError('No selfstarring allowed')

            await self.raw_add_star(config, message, author_id)
            await self.update_star(config, message)
        finally:
            lock.release()

    async def remove_star(self,
                          message: discord.Message,
                          author_id: int,
                          config: dict = None):
        """Remove a star from a message.

        Parameters
        ----------
        message: `discord.Message`
            Message.
        author_id: int
            ID of the person that is getting their star removed.

        Raises
        ------
        StarRemoveError
            Any kind of error while remoing the star.
        """
        lock = self._locks[message.guild.id]
        await lock

        try:
            if not config:
                config = await self._get_starconfig(message.guild.id)

            await self.check_allow(config, message.channel.id)

            if hasattr(author_id, 'id'):
                author_id = author_id.id

            if author_id == message.author.id:
                raise StarRemoveError('No selfstarring allowed')

            await self.raw_remove_star(config, message, author_id)
            await self.update_star(config, message)
        finally:
            lock.release()

    async def remove_all(self, message: discord.Message, config: dict = None):
        """Remove all stars from a message.

        Parameters
        ----------
        message: `discord.Message`
            Message that is going to have all stars removed.
        """
        lock = self._locks[message.guild.id]
        await lock

        try:
            if not config:
                config = await self._get_starconfig(message.guild.id)

            await self.raw_remove_all(config, message)
            await self.update_star(config, message, True)
        finally:
            lock.release()

    async def delete_starconfig(self, config: dict,
                                delete_stars=False) -> bool:
        """Deletes a starboard configuration from the collection.

        Returns
        -------
        bool
            Success/Failure of the operation.
        """
        guild = self.bot.get_guild(config['guild_id'])
        log.debug('Deleting starconfig for %s[%d]', guild.name, guild.id)

        if delete_stars:
            del_res = await self.pool.execute("""
            DELETE FROM starboard
            WHERE guild_id = $1
            """)

            log.warning(f'starconfig del with stars del: {del_res}')

            res = await self.pool.execute("""
            DELETE FROM starboard_config
            WHERE guild_id = $1
            """, guild.id)
        else:
            res = await self.pool.execute("""
            UPDATE starboard_config
            SET enabled = false
            WHERE guild_id = $1
            """, guild.id)

        # gotta keep that api compatibility
        _, deleted = res.split(' ')
        deleted = int(deleted)
        return deleted > 0

    def check_star(self, cfg: dict,
                   emoji_partial: discord.PartialEmoji,
                   user_id: int) -> bool:
        """Check if the given partial reaction data
        match the starboard configuration data for custom star emotes.

        Also checks if the star author is a bot.
        """

        author = self.bot.get_user(user_id)
        if author is not None and author.bot:
            return False

        star_emoji = cfg.get('star_emoji', DEFAULT_STAR_EMOJI)

        # check unicode
        if emoji_partial.name == star_emoji:
            return True

        # check custom emotes (by id)
        try:
            emoji_id = int(star_emoji)
            if emoji_partial.id == emoji_id:
                return True
        except ValueError:
            pass

        return False

    async def check_allow(self, cfg: dict, channel_id: int):
        """Check if the current channel is allowed to have
        messages starred from."""
        allowed_chans = await self._get_allowed_chans(cfg['guild_id'])

        if not allowed_chans:
            return

        try:
            allowed_chans.index(channel_id)
        except ValueError:
            raise StarError('Channel not allowed to be starred')

    async def _sbhandle(self, message, sbctx, cfg):
        channel_id = sbctx['channel_id']
        user_id = sbctx['user_id']

        if channel_id == cfg['starboard_id'] and \
                user_id != self.bot.user.id:
            # This reaction is coming from the starboard.
            content = message.content
            log.debug(f'parsing things out from {content!r}')

            matches = ID_REGEX.findall(content)
            try:
                new_message_id = int(matches[-1])
                new_channel_id = int(matches[-2])
                return new_message_id, new_channel_id
            except (IndexError, ValueError):
                # no matches found, rip.
                log.warning(f'[sbhandle] failure parsing {content!r}')
                return None, None

        return None, None

    async def on_raw_reaction_add(self, payload):
        """Handle a reaction add."""
        emoji_partial = payload.emoji
        message_id = payload.message_id
        channel_id = payload.channel_id
        user_id = payload.user_id

        channel = self.bot.get_channel(channel_id)
        if not channel:
            return

        if isinstance(channel, discord.DMChannel):
            return

        cfg = await self.get_starconfig(channel.guild.id)
        if not cfg:
            return

        is_star = self.check_star(cfg, emoji_partial, user_id)
        if not is_star:
            return

        # ignore blocked people
        if await self.bot.is_blocked_guild(channel.guild.id) or \
                await self.bot.is_blocked(user_id):
            return

        try:
            await self.check_allow(cfg, channel_id)
            message = await channel.get_message(message_id)

            new_message_id, new_channel_id = await self._sbhandle(
                message, {
                    'channel_id': channel_id,
                    'user_id': user_id,
                }, cfg)

            if new_message_id and new_channel_id:
                payload = RawReactionActionEvent({
                    'message_id': new_message_id,
                    'channel_id': new_channel_id,
                    'user_id': user_id,
                }, emoji_partial)

                return await self.on_raw_reaction_add(payload)

            await self.add_star(message, user_id, cfg)
        except (StarError, StarAddError) as err:
            log.warning(f'raw_reaction_add: {err!r}')
        except Exception:
            log.exception('add_star @ reaction_add, %s[cid=%d] %s[gid=%d]',
                          channel.name, channel.id, channel.guild.name,
                          channel.guild.id)

    async def on_raw_reaction_remove(self, payload):
        emoji_partial = payload.emoji
        message_id = payload.message_id
        channel_id = payload.channel_id
        user_id = payload.user_id
        channel = self.bot.get_channel(channel_id)
        if not channel:
            return

        if isinstance(channel, discord.DMChannel):
            return

        cfg = await self.get_starconfig(channel.guild.id)
        if not cfg:
            return

        is_star = self.check_star(cfg, emoji_partial, user_id)
        if not is_star:
            return

        # ignore blocked people
        if await self.bot.is_blocked_guild(channel.guild.id) or \
                await self.bot.is_blocked(user_id):
            return

        try:
            await self.check_allow(cfg, channel_id)
            message = await channel.get_message(message_id)

            new_message_id, new_channel_id = await self._sbhandle(
                message, {
                    'channel_id': channel_id,
                    'user_id': user_id,
                }, cfg)

            if new_message_id and new_channel_id:
                payload = RawReactionActionEvent({
                    'message_id': new_message_id,
                    'channel_id': new_channel_id,
                    'user_id': user_id,
                }, emoji_partial)

                return await self.on_raw_reaction_remove(payload)

            await self.remove_star(message, user_id, cfg)
        except (StarError, StarRemoveError) as err:
            log.warning(f'raw_reaction_remove: {err!r}')
        except Exception:
            log.exception('reaction_remove, %s[cid=%d] %s[gid=%d]',
                          channel.name, channel.id, channel.guild.name,
                          channel.guild.id)

    async def on_raw_reaction_clear(self, payload):
        """Remove all stars in the message."""
        message_id = payload.message_id
        channel_id = payload.channel_id

        channel = self.bot.get_channel(channel_id)
        if not channel:
            return

        if isinstance(channel, discord.DMChannel):
            return

        cfg = await self.get_starconfig(channel.guild.id)
        if not cfg:
            return

        # ignore blocked stuff
        if await self.bot.is_blocked_guild(channel.guild.id):
            return

        try:
            message = await channel.get_message(message_id)
            await self.remove_all(message, cfg)
        except (StarError, StarRemoveError) as err:
            log.warning(f'raw_reaction_clear: {err!r}')
        except Exception:
            log.exception('remove_all @ reaction_clear, %s[cid=%d] %s[gid=%d]',
                          channel.name, channel.id, channel.guild.name,
                          channel.guild.id)

    @commands.command()
    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    async def starboard(self, ctx, channel_name: str):
        """Create a starboard channel.

        If the name specifies a NSFW channel,
        the starboard gets marked as NSFW.

        NSFW starboards allow messages from NSFW
        channels to be starred without any censoring.

        If your starboard gets marked as a SFW starboard,
        messages from NSFW channels get completly ignored.
        """

        guild = ctx.guild
        config = await self.get_starconfig(guild.id)
        if config is not None:
            await ctx.send("You already have a starboard. If you want"
                           " to detach josé from it, use the "
                           "`stardetach` command")
            return

        po = discord.PermissionOverwrite
        overwrites = {
            guild.default_role: po(read_messages=True, send_messages=False),
            guild.me: po(read_messages=True, send_messages=True),
        }

        try:
            starboard_chan = await guild.create_text_channel(
                channel_name,
                overwrites=overwrites,
                reason='Created starboard channel')

        except discord.Forbidden:
            return await ctx.send('No permissions to make a channel.')
        except discord.HTTPException as err:
            log.exception('Got HTTP error from starboard create')
            return await ctx.send(f'**SHIT!!!!**:  {err!r}')

        log.info(f'[starboard] Init starboard @ {guild.name}[{guild.id}]')

        # create config here
        await self.pool.execute("""
        INSERT INTO starboard_config (guild_id, starboard_id)
        VALUES ($1, $2)
        """, guild.id, starboard_chan.id)

        await self._sb_setup(guild.id, starboard_chan.id)
        await ctx.ok()

    async def _sb_setup(self, guild_id: int, starboard_id: int):
        """Create or enable a starboard configuration."""
        try:
            await self.pool.execute("""
            INSERT INTO starboard_config (guild_id, starboard_id)
            VALUES ($1, $2)
            """, guild_id, starboard_id)
        except asyncpg.UniqueViolationError:
            await self.pool.execute("""
            UPDATE starboard_config
            SET enabled = true, starboard_id = $1
            WHERE guild_id = $2
            """, starboard_id, guild_id)

    @commands.command()
    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    async def starattach(self, ctx, starboard_chan: discord.TextChannel):
        """Attach an existing channel as a starboard.

        With this command you can create your starboard
        without needing José to automatically create the starboard for you
        """
        config = await self.get_starconfig(ctx.guild.id)
        if config:
            return await ctx.send('You already have a starboard config setup.')

        await self._sb_setup(ctx.guild.id, starboard_chan.id)
        await ctx.ok()

    @commands.command()
    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    async def stardetach(self, ctx, confirm: bool = False):
        """Detaches José from your starboard.

        Detaching means José will remove your starboard's configuration.
        And will stop detecting starred/unstarred posts, etc.

        Provide "y" as your confirmation.

        Manage Guild permission is required.
        """
        if not confirm:
            return await ctx.send('Operation not confirmed by user.')

        config = await self._get_starconfig(ctx.guild.id)
        await ctx.success(await self.delete_starconfig(config))

    @commands.command()
    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    async def stardelete(self, ctx, confirm: bool = False):
        """Completly delete all starboard data from the guild.

        Follows the same logic as `j!stardetach`, but it
        deletes all starboard data, not just the configuration.
        """
        if confirm != 'y':
            return await ctx.send('not confirmed')

        config = await self._get_starconfig(ctx.guild.id)
        await self.delete_starconfig(config, True)

    @commands.command()
    @commands.guild_only()
    async def star(self, ctx, message_id: int):
        """Star a message."""
        try:
            message = await ctx.channel.get_message(message_id)
        except discord.NotFound:
            return await ctx.send('Message not found in the current channel')
        except discord.Forbidden:
            return await ctx.send("Can't retrieve message")
        except discord.HTTPException as err:
            return await ctx.send(f'Failed to retrieve message: {err!r}')

        try:
            await self.add_star(message, ctx.author)
            await ctx.ok()
        except (StarAddError, StarError) as err:
            log.warning(f'[star_command] Errored: {err!r}')
            return await ctx.send(f'Failed to add star: {err!r}')

    @commands.command()
    @commands.guild_only()
    async def unstar(self, ctx, message_id: int):
        """Unstar a message."""
        try:
            message = await ctx.channel.get_message(message_id)
        except discord.NotFound:
            return await ctx.send('Message not found in the current channel')
        except discord.Forbidden:
            return await ctx.send("Can't retrieve message")
        except discord.HTTPException as err:
            return await ctx.send(f'Failed to retrieve message: {err!r}')

        try:
            await self.remove_star(message, ctx.author)
            await ctx.ok()
        except (StarRemoveError, StarError) as err:
            log.warning(f'[unstar_cmd] Errored: {err!r}')
            return await ctx.send(f'Failed to remove star: {err!r}')

    @commands.command()
    @commands.guild_only()
    async def starrers(self, ctx, message_id: int):
        """Get the list of starrers from a message in the current channel."""
        guild = ctx.guild
        await self._get_starconfig(guild.id)
        star = await self.get_star(guild.id, message_id)
        if not star:
            return await ctx.send('Star object not found')

        channel = self.bot.get_channel(star['channel_id'])
        if not channel:
            return await ctx.send('Star found, Channel not found')

        try:
            message = await channel.get_message(message_id)
        except discord.NotFound:
            return await ctx.send('Message not found in the channel')
        except discord.Forbidden:
            return await ctx.send("Can't retrieve message")
        except discord.HTTPException as err:
            return await ctx.send(f'Failed to retrieve message: {err!r}')

        _, em = await self._star_embed(message)

        starrers = await self.get_starrers(message.id)
        starrers = map(lambda sid: (guild.get_member(sid), sid), starrers)

        def try_name(m, uid: int) -> str:
            """Try to get a name for a member."""
            if m is None:
                return f'Unfindable {uid}'

            return m.display_name

        starrer_list = (try_name(m[0], m[1]) for m in starrers)
        em.add_field(name='Starrers', value=', '.join(starrer_list))
        await ctx.send(embed=em)

    @commands.command()
    @commands.guild_only()
    async def starstats(self, ctx):
        """Get statistics about your starboard."""
        # This function is true hell.
        await self._get_starconfig(ctx.guild.id)

        em = discord.Embed(
            title='Starboard statistics', colour=discord.Colour(0xFFFF00))

        total_messages = await self.pool.fetchval("""
        SELECT COUNT(*)
        FROM starboard
        WHERE guild_id = $1
        """, ctx.guild.id)
        em.add_field(name='Total messages starred', value=total_messages)

        starrers = collections.Counter()
        authors = collections.Counter()

        top_stars = await self.pool.fetch("""
        SELECT
            starboard.message_id,
            starboard.channel_id,
            starboard.author_id,
            COUNT(starboard_starrers.starrer_id) AS starrers
        FROM
            starboard
        JOIN starboard_starrers
        ON starboard.message_id = starboard_starrers.message_id
        WHERE starboard.guild_id = $1
        GROUP BY starboard.message_id
        ORDER BY starrers DESC
        LIMIT 5
        """, ctx.guild.id)

        # people who starred the most / received stars the most
        con = await self.pool.acquire()
        async with con.transaction():
            cur = con.cursor("""
            SELECT message_id, author_id
            FROM starboard
            WHERE guild_id = $1
            """, ctx.guild.id)

            async for row in cur:
                try:
                    authors[row['author_id']] += 1
                except KeyError:
                    pass

                sstarrers = await self.get_starrers(row['message_id'])
                for starrer_id in sstarrers:
                    starrers[starrer_id] += 1

        await self.pool.release(con)

        # process top 5
        res_sm = []
        for (idx, star) in enumerate(top_stars):
            if 'author_id' not in star:
                continue

            starcount = star['starrers']

            stctx = (f'<@{star["author_id"]}> - {star["message_id"]} '
                     f'@ <#{star["channel_id"]}> '
                     f'({starcount} stars)')

            res_sm.append(f'{idx + 1}\N{COMBINING ENCLOSING KEYCAP} {stctx}')

        em.add_field(
            name='Most starred messages',
            value='\n'.join(res_sm) or '-- None --',
            inline=False)

        # process people who received stars the most
        mc_receivers = authors.most_common(5)
        res_sr = []

        for idx, data in enumerate(mc_receivers):
            user_id, received_stars = data

            # ALWAYS make sure the member is in the guild.
            member = ctx.guild.get_member(user_id)
            if not member:
                continue

            auctx = f'<@{user_id}> ({received_stars} stars)'
            res_sr.append(f'{idx + 1}\N{COMBINING ENCLOSING KEYCAP} {auctx}')

        em.add_field(
            name='Top 5 Star Receivers',
            value='\n'.join(res_sr) or '-- None --',
            inline=False)

        # process people who *gave* stars the most
        mc_givers = starrers.most_common(5)
        res_gr = []

        for idx, data in enumerate(mc_givers):
            member_id, star_count = data
            member = ctx.guild.get_member(member_id)
            if not member:
                continue

            srctx = f'{member.mention} ({star_count} stars)'
            res_gr.append(f'{idx + 1}\N{COMBINING ENCLOSING KEYCAP} {srctx}')

        em.add_field(
            name=f'Top 5 Star Givers', value='\n'.join(res_gr), inline=False)

        await ctx.send(embed=em)

    @commands.command(aliases=['rs'])
    @commands.guild_only()
    async def randomstar(self, ctx):
        """Get a random star from your starboard."""
        guild = ctx.guild
        await self._get_starconfig(ctx.guild.id)

        star = await self.pool.fetchrow(f"""
        SELECT message_id, channel_id, guild_id, author_id
        FROM starboard
        WHERE starboard.guild_id = $1
        ORDER BY RANDOM()
        LIMIT 1
        """, guild.id)

        if star is None:
            return await ctx.send('No star object found')

        channel = self.bot.get_channel(star['channel_id'])
        if channel is None:
            return await ctx.send('Star references a non-findable channel.')

        message_id = star['message_id']
        try:
            message = await channel.get_message(message_id)
        except discord.NotFound:
            raise self.SayException('Message not found')
        except discord.Forbidden:
            raise self.SayException("Can't retrieve message")
        except discord.HTTPException as err:
            raise self.SayException(f'Failed to retrieve message: {err!r}')

        current = ctx.channel.is_nsfw()
        schan = channel.is_nsfw()
        if not current and schan:
            raise self.SayException(f'channel nsfw={current}, '
                                    f'nsfw={schan}, nope')

        title, embed = await self._star_embed(message)
        await ctx.send(title, embed=embed)

    @commands.command()
    @commands.guild_only()
    async def streload(self, ctx, message_id: int):
        """Star reload.

        Reload a message, its starrers and update the star in the starboard.
        Useful if the starred message was edited.
        """
        channel = ctx.channel
        cfg = await self._get_starconfig(channel.guild.id)

        try:
            message = await channel.get_message(message_id)
        except discord.NotFound:
            raise self.SayException('Message not found in the current channel')

        star = await self.get_star(ctx.guild.id, message_id)
        if not star:
            raise self.SayException('Star object not found')

        try:
            await self.update_star(cfg, message)
        except StarError as err:
            log.error(f'force_reload: {err!r}')
            raise self.SayException(f'rip {err!r}')

        await ctx.ok()

    @commands.command()
    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    async def sbsetc(self, ctx, emoji: str = None):
        """Set a custom emote (or unicode emoji) as your starboard emote.

        This does not check against bad values (like "a")
        which are un-reactable. Use with caution.

        Only people with the "Manage Server" permission
        can use this command.
        """
        config = await self._get_starconfig(ctx.guild.id)

        if not emoji:
            emoji = config.get('star_emoji', DEFAULT_STAR_EMOJI)
            try:
                emoji = self.bot.get_emoji(int(emoji))
            except ValueError:
                pass

            return await ctx.send(f'The starboard emote is {str(emoji)}')

        match = EMOJI_REGEX.match(emoji)

        custom = bool(match)
        emoji_res = ''

        if not custom:
            emoji_res = emoji
        else:
            try:
                emoji_res = match.group(1)
                int(emoji_res)
            except ValueError:
                raise self.SayException(':x: Custom Emote ID is not a number')

        await self.pool.execute("""
        UPDATE starboard_config
        SET star_emoji = $1
        WHERE guild_id = $2
        """, emoji_res, ctx.guild.id)

        await ctx.ok()

    async def _get_allowed_chans(self, guild_id: int) -> list:
        chan_ids = await self.pool.fetch("""
        SELECT channel_id
        FROM starconfig_allow
        WHERE guild_id = $1
        """, guild_id)

        return [r['channel_id'] for r in chan_ids]

    @commands.command()
    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    async def sbtoggle(self, ctx, channel: discord.TextChannel):
        """Toggle a channel's allowance on starboard.

        By default, all channels are allowed to have
        their messages starred.

        As soon as you filter it to be at least 1 channel,
        all the others will become blocked by default.
        """
        await self._get_starconfig(ctx.guild.id)

        allowed_chans = await self._get_allowed_chans(ctx.guild.id)

        if channel.id in allowed_chans:
            await self.pool.execute("""
            DELETE FROM starconfig_allow
            WHERE guild_id = $1 AND channel_id = $2
            """, ctx.guild.id, channel.id)
            await ctx.send(f'<#{channel.id}> is **disallowed** to be starred')
        else:
            await self.pool.execute("""
            INSERT INTO starconfig_allow (guild_id, channel_id)
            VALUES ($1, $2)
            """, ctx.guild.id, channel.id)
            await ctx.send(f'<#{channel.id}> is **allowed** to be starred')

        allowed_chans = await self._get_allowed_chans(ctx.guild.id)

        if not allowed_chans:
            await ctx.send('All channels are available to be starred')

        await ctx.ok()

    @commands.command()
    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    async def sbthreshold(self, ctx, stars: int):
        """Set a threshold for stars to enter starboard.
        """
        await self._get_starconfig(ctx.guild.id)

        if stars < 1:
            raise self.SayException('Invalid threshold.')

        await self.pool.execute("""
        UPDATE starboard_config
        SET star_threshold = $1
        WHERE guild_id = $2
        """, stars, ctx.guild.id)

        await ctx.ok()


def setup(bot):
    bot.add_jose_cog(Starboard)
