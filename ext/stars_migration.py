import logging

import discord
from discord.ext import commands

from .common import Cog
from .stars import DEFAULT_STAR_EMOJI

log = logging.getLogger(__name__)


class StarboardMigration(Cog):
    async def _migrate_sbconfig(self, cfg: dict):
        guild_id = cfg['guild_id']

        guild = self.bot.get_guild(guild_id)
        if not guild:
            return

        if isinstance(cfg.get('star_emoji', DEFAULT_STAR_EMOJI), int):
            cfg['star_emoji'] = str(cfg['star_emoji'])

        await self.pool.execute(
            """
            INSERT INTO starboard_config (guild_id, starboard_id,
                                        star_emoji, star_threshold)
            VALUES ($1, $2, $3, $4)
            """, guild_id, cfg['starboard_id'],
            cfg.get('star_emoji', DEFAULT_STAR_EMOJI),
            cfg.get('threshold', 1))

        self._sbguilds.append(cfg['guild_id'])

        for chan_id in cfg.get('allowed_chans', []):
            await self.pool.execute("""
            INSERT INTO starconfig_allow (guild_id, channel_id)
            VALUES ($1, $2)
            """, guild_id, chan_id)

    async def _migrate_star(self, star: dict):
        guild_id = star['guild_id']

        guild = self.bot.get_guild(guild_id)
        if not guild:
            return

        # quicker than requerying
        if guild_id not in self._sbguilds:
            return

        chan = self.bot.get_channel(star['channel_id'])
        if not chan:
            return

        if 'author_id' not in star:
            try:
                msg = await chan.get_message(star['message_id'])
                star['author_id'] = msg.author.id
            except (discord.errors.NotFound, discord.errors.Forbidden):
                # if message not found, dont migrate star
                return

        # main migrate
        await self.pool.execute(
            """
            INSERT INTO starboard (message_id, channel_id,
                                   guild_id, author_id, star_message_id)
            VALUES ($1, $2, $3, $4, $5)
            """,
            star['message_id'], star['channel_id'],
            star['guild_id'], star['author_id'],
            star.get('star_message_id'))

        # migrate starrers
        for starrer_id in star['starrers']:
            await self.pool.execute("""
            INSERT INTO starboard_starrers (message_id, starrer_id)
            VALUES ($1, $2)
            """, star['message_id'], starrer_id)

    @commands.command()
    @commands.is_owner()
    async def sbmigrate(self, ctx):
        await ctx.send('hewwo welcome to stawboawd migwation owo')
        await ctx.send('FUCKY mongodb òwó')

        sb_cfg_coll = self.config.jose_db['starconfig']
        sb_star_coll = self.config.jose_db['starboard']
        self._sbguilds = []

        await ctx.send('Step 1: migrating starboard configurations')

        cfg_count, cfg_success = 0, 0
        async for cfg in sb_cfg_coll.find():
            cfg_count += 1
            try:
                await self._migrate_sbconfig(cfg)
                cfg_success += 1
            except:
                log.exception('failed to migrate')

        await ctx.send(f'success starconfig: {cfg_success} out of {cfg_count}')
        await ctx.send(f'Step 2: migrating stars')

        star_count, star_succ = 0, 0
        async for star in sb_star_coll.find():
            star_count += 1
            try:
                await self._migrate_star(star)
                star_succ += 1
            except:
                log.exception('failed to migrate')

        await ctx.send(f'success stars: {star_succ} / {star_count}')


def setup(bot):
    bot.add_jose_cog(StarboardMigration)
