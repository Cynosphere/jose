yarl<1.2
git+https://github.com/Rapptz/discord.py@rewrite

asyncpg==0.17.0
motor==2.0.0
uvloop==0.11.2

markovify==0.7.1
psutil==5.4.7
wolframalpha==3.0.1
pyowm==2.9.0
Pillow==5.2.0
midiutil==1.1.3
chatterbot==0.7.6
